Opdracht om op een correcte manier multithreading toe te passen in dit Java programma. Het betreft een resaurant simulatie, waarbij er meerdere koks en obers bestellingen en maaltijden kunnen klaarmaken. Elke kok en ober krijgt een eigen thread, waardoor deze tegelijkertijd kunnen lopen. Zolang er bestellingen zijn, pakt de kok steeds een bestelling, bereidt deze, en plaatst de maaltijd vervolgens op de balie. De ober pakt vervolgens deze maaltijd en bezorgt deze. Om de simulatie wat realistischer te maken zijn tijden geïntroduceerd, zoals een variabele bereidingstijd en een constante bezorgtijd. 


OPDRACHT 1: Analyse van de uitgangssituatie
Bestudeer het Java-project en teken het ontwerpklassediagram dat hoort
bij deze implementatie.
We gaan het simulatieprogramma nu meer realistisch maken. In plaats
van één kok en één ober laten we nu meerdere koks en obers werken die
gelijktijdig hun werk uitvoeren.
OPDRACHT 2: Analyse en ontwerp van extra functionaliteit
Bereid het ontwerpklassediagram uit zodat deze functionaliteit wordt
gerealiseerd. Aandachtspunten zijn:
- Waar kunnen koks, obers en threads het beste worden gecreeerd?
- Kunnen door het het invoeren van threads atomiciteitsproblemen
ontstaan? Zo ja, hoe worden deze opgelost?

OPDRACHT 3: Implementatie
Implementeer nu het ontwerp en run het een aantal keren.
